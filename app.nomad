job "alerts_home_lan" {
    datacenters = ["primary"]
    type = "service"

    group "ntfy" {
        network {
            port "http" {
                to = 80
                host_network = "private"
            }
        }

        service {
            provider = "nomad"
            port = "http"
            name = "ntfy"

            tags = [
                "traefik.enable=true",
                "traefik.http.routers.ntfy.rule=Host(`alerts.home.lan`)"
            ]

            check {
                name = "ntfy HTTP"
                type = "tcp"
                port = "http"
                interval = "10s"
                timeout = "2s"
            }

            check_restart {
                limit = 3
                grace = "90s"
                ignore_warnings = false
            }
        }

        task "run" {
            driver = "docker"

            config {
                image = "binwiederhier/ntfy:v2.5.0"
                args = ["serve"]
                network_mode = "bridge"

                ports = ["http"]

                volumes = [
                    "/opt/nomad/storage/alerts.home.lan/cache:/var/cache/ntfy",
                    "/opt/nomad/storage/alerts.home.lan/config:/etc/ntfy",
                    "/opt/nomad/storage/alerts.home.lan/logs:/var/log"
                ]
            }

            resources {
                cpu = 300
                memory = 512
            }
        }
    }
}
